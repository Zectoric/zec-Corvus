#include <iostream>
#include "corv.hpp"

using namespace corv;

int main()
{
	Array arr;
	arr.append('m');
	arr.append('m');
	arr.append('m');
	arr.append('m');
	arr.append('m');
	Array search;
	search.append('l');
	search.append('l');
	arr = arr.repl('m', search);
	std::cout << "{ ";
	for (int i = 0; i < arr.len(); i++)
	{
		if (i)
			std::cout << ", ";
		corv::Int I = arr[i];
		std::cout << I;
	}
	std::cout << " }" << std::endl;
	std::cout << arr.lastIndexOf(search, 2);
	return 0;
}
