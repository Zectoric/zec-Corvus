#ifndef CORV_H
#define CORV_H

#include <iostream>

namespace corv
{
	enum class Type
	{
		Int,
		Long,
		Unsigned,
		UnsignedLong,
		Short,
		UnsignedShort,
		Char,
		String,
		Float,
		Double,
		LongDouble
	};
	class Var
	{
	public:
		/*
		
		*******************
		*Class Declarations*
		*******************
		
		*/
		class BasicInt;
		class BasicLong;
		class BasicUnsigned;
		class BasicUnsignedLong;
		class BasicShort;
		class BasicUnsignedShort;
		class BasicChar;
		class BasicString;
		class BasicFloat;
		class BasicDouble;
		class BasicLongDouble;

		/*

		*******************
		*Class Definitions*
		*******************

		*/
		class BasicInt
		{
		public:
			BasicInt();
			BasicInt(const int&);
			BasicInt(const BasicInt&);
			BasicInt(const int&, const int&);

			int& val() const;
			BasicInt& val(const int&) const;
			BasicInt& val(const BasicInt&) const;
			BasicInt& val(const int&, const int&) const;

			BasicInt absolute() const;

			BasicInt toBase(const BasicInt&) const;
			BasicString toBaseString(const BasicInt&) const;

			BasicInt pow() const;
			BasicInt pow(const BasicInt&) const;

			BasicInt& operator+=(const BasicInt&) const;
			BasicInt& operator-=(const BasicInt&) const;
			BasicInt& operator*=(const BasicInt&) const;
			BasicInt& operator/=(const BasicInt&) const;
			BasicInt& operator%=(const BasicInt&) const;

			BasicInt& operator++() const;
			BasicInt& operator--() const;
			BasicInt& operator++(int) const;
			BasicInt& operator--(int) const;

			BasicInt& operator=(const int&) const;
			BasicInt& operator=(const BasicInt&) const;

			operator int() const;

			Type type() const;

		private:
			mutable int v;
		};

		class BasicLong
		{
		public:
			BasicLong();
			BasicLong(const long&);
			BasicLong(const BasicLong&);
			BasicLong(const long&, const int&);

			long& val() const;
			BasicLong& val(const long&) const;
			BasicLong& val(const BasicLong&) const;
			BasicLong& val(const long&, const int&) const;

			BasicLong absolute() const;

			BasicLong toBase(const BasicInt&) const;
			BasicString toBaseString(const BasicInt&) const;

			BasicLong pow() const;
			BasicLong pow(const BasicLong&) const;

			BasicLong& operator+=(const BasicLong&) const;
			BasicLong& operator-=(const BasicLong&) const;
			BasicLong& operator*=(const BasicLong&) const;
			BasicLong& operator/=(const BasicLong&) const;
			BasicLong& operator%=(const BasicLong&) const;

			BasicLong& operator++() const;
			BasicLong& operator--() const;
			BasicLong& operator++(int) const;
			BasicLong& operator--(int) const;

			BasicLong& operator=(const long&) const;
			BasicLong& operator=(const BasicLong&) const;

			operator long() const;

			Type type() const;

		private:
			mutable long v;
		};

		class BasicUnsigned
		{
		public:
			BasicUnsigned();
			BasicUnsigned(const unsigned&);
			BasicUnsigned(const BasicUnsigned&);
			BasicUnsigned(const unsigned&, const int&);

			unsigned& val() const;
			BasicUnsigned& val(const unsigned& value) const;
			BasicUnsigned& val(const BasicUnsigned& value) const;
			BasicUnsigned& val(const unsigned& value, const int& b) const;


			BasicUnsigned absolute() const;

			BasicUnsigned toBase(const BasicInt&) const;
			BasicString toBaseString(const BasicInt&) const;

			BasicUnsigned pow() const;
			BasicUnsigned pow(const BasicUnsigned&) const;

			BasicUnsigned& operator+=(const BasicUnsigned&) const;
			BasicUnsigned& operator-=(const BasicUnsigned&) const;
			BasicUnsigned& operator*=(const BasicUnsigned&) const;
			BasicUnsigned& operator/=(const BasicUnsigned&) const;
			BasicUnsigned& operator%=(const BasicUnsigned&) const;

			BasicUnsigned& operator++() const;
			BasicUnsigned& operator--() const;
			BasicUnsigned& operator++(int) const;
			BasicUnsigned& operator--(int) const;

			BasicUnsigned& operator=(const unsigned&) const;
			BasicUnsigned& operator=(const BasicUnsigned&) const;

			operator unsigned() const;

			Type type() const;

		private:
			mutable unsigned v;
		};

		class BasicUnsignedLong
		{
		public:
			BasicUnsignedLong();
			BasicUnsignedLong(const unsigned long&);
			BasicUnsignedLong(const BasicUnsignedLong&);
			BasicUnsignedLong(const unsigned long&, const int&);

			unsigned long& val() const;
			BasicUnsignedLong& val(const unsigned long&) const;
			BasicUnsignedLong& val(const BasicUnsignedLong&) const;
			BasicUnsignedLong& val(const unsigned long&, const int&) const;

			BasicUnsignedLong absolute() const;

			BasicUnsignedLong toBase(const BasicInt&) const;
			BasicString toBaseString(const BasicInt&) const;

			BasicUnsignedLong pow() const;
			BasicUnsignedLong pow(const BasicUnsignedLong&) const;

			BasicUnsignedLong& operator+=(const BasicUnsignedLong&) const;
			BasicUnsignedLong& operator-=(const BasicUnsignedLong&) const;
			BasicUnsignedLong& operator*=(const BasicUnsignedLong&) const;
			BasicUnsignedLong& operator/=(const BasicUnsignedLong&) const;
			BasicUnsignedLong& operator%=(const BasicUnsignedLong&) const;

			BasicUnsignedLong& operator++() const;
			BasicUnsignedLong& operator--() const;
			BasicUnsignedLong& operator++(int) const;
			BasicUnsignedLong& operator--(int) const;

			BasicUnsignedLong& operator=(const unsigned long&) const;
			BasicUnsignedLong& operator=(const BasicUnsignedLong&) const;

			operator unsigned long() const;

			Type type() const;

		private:
			mutable unsigned long v;
		};

		class BasicShort
		{
		public:
			BasicShort();
			BasicShort(const short&);
			BasicShort(const BasicShort&);
			BasicShort(const short&, const int&);

			short& val() const;
			BasicShort& val(const short&) const;
			BasicShort& val(const BasicShort&) const;
			BasicShort& val(const short&, const int&) const;

			BasicShort absolute() const;

			BasicShort toBase(const BasicInt&) const;
			BasicString toBaseString(const BasicInt&) const;

			BasicShort pow() const;
			BasicShort pow(const BasicShort&) const;

			BasicShort& operator+=(const BasicShort&) const;
			BasicShort& operator-=(const BasicShort&) const;
			BasicShort& operator*=(const BasicShort&) const;
			BasicShort& operator/=(const BasicShort&) const;
			BasicShort& operator%=(const BasicShort&) const;

			BasicShort& operator++() const;
			BasicShort& operator--() const;
			BasicShort& operator++(int) const;
			BasicShort& operator--(int) const;

			BasicShort& operator=(const short&) const;
			BasicShort& operator=(const BasicShort&) const;

			operator short() const;

			Type type() const;

		private:
			mutable short v;
		};

		class BasicUnsignedShort
		{
		public:
			BasicUnsignedShort();
			BasicUnsignedShort(const unsigned short&);
			BasicUnsignedShort(const BasicUnsignedShort&);
			BasicUnsignedShort(const unsigned short&, const int&);

			unsigned short& val() const;
			BasicUnsignedShort& val(const unsigned short&) const;
			BasicUnsignedShort& val(const BasicUnsignedShort&) const;
			BasicUnsignedShort& val(const unsigned short&, const int&) const;

			BasicUnsignedShort absolute() const;

			BasicUnsignedShort toBase(const BasicInt&) const;
			BasicString toBaseString(const BasicInt&) const;

			BasicUnsignedShort pow() const;
			BasicUnsignedShort pow(const BasicUnsignedShort&) const;

			BasicUnsignedShort& operator+=(const BasicUnsignedShort&) const;
			BasicUnsignedShort& operator-=(const BasicUnsignedShort&) const;
			BasicUnsignedShort& operator*=(const BasicUnsignedShort&) const;
			BasicUnsignedShort& operator/=(const BasicUnsignedShort&) const;
			BasicUnsignedShort& operator%=(const BasicUnsignedShort&) const;

			BasicUnsignedShort& operator++() const;
			BasicUnsignedShort& operator--() const;
			BasicUnsignedShort& operator++(int) const;
			BasicUnsignedShort& operator--(int) const;

			BasicUnsignedShort& operator=(const unsigned short&) const;
			BasicUnsignedShort& operator=(const BasicUnsignedShort&) const;

			operator unsigned short() const;

			Type type() const;

		private:
			mutable unsigned short v;
		};

		class BasicChar
		{
		public:
			BasicChar();
			BasicChar(const char&);
			BasicChar(const BasicChar&);
			BasicChar(const BasicInt&);

			Type type() const;

			char& val() const;
			BasicChar& val(const char&) const;
			BasicChar& val(const BasicChar&) const;
			BasicChar& val(const BasicInt&) const;

			bool isAlnum() const;

			bool isAlpha() const;

			bool isDigit() const;

			bool isLower() const;

			bool isPunct() const;

			bool isSpace() const;

			bool isUpper() const;

			BasicChar toLower() const;

			BasicChar toUpper() const;

			BasicChar operator+(const BasicChar&) const;
			BasicChar operator-(const BasicChar&) const;
			BasicChar operator*(const BasicChar&) const;
			BasicChar operator/(const BasicChar&) const;

			BasicChar& operator+=(const BasicChar&) const;
			BasicChar& operator-=(const BasicChar&) const;
			BasicChar& operator*=(const BasicChar&) const;
			BasicChar& operator/=(const BasicChar&) const;

			bool operator<(const BasicChar&) const;
			bool operator>(const BasicChar&) const;

			bool operator<=(const BasicChar&) const;
			bool operator>=(const BasicChar&) const;

			BasicChar& operator=(const char&) const;
			BasicChar& operator=(const BasicChar&) const;
			BasicChar& operator=(const BasicInt&) const;

			operator char() const;
			operator int() const;

		private:
			mutable char v;
		};

		class BasicString
		{
		public:
			BasicString();
			BasicString(const char*);
			BasicString(const std::string&);
			BasicString(const BasicString&);

			BasicString(const char*, const BasicInt&);
			BasicString(const std::string&, const BasicInt&);
			BasicString(const BasicString&, const BasicInt&);

			BasicString(const char*, const BasicInt&, const BasicInt&);
			BasicString(const std::string&, const BasicInt&, const BasicInt&);
			BasicString(const BasicString&, const BasicInt&, const BasicInt&);

			BasicString(const BasicChar&);
			~BasicString();

			Type type() const;

			char*& val() const;
			BasicString& val(const BasicString&) const;
			BasicString& val(const BasicString&, const BasicInt&) const;
			BasicString& val(const BasicString&, const BasicInt&, const BasicInt&) const;

			BasicInt& len() const;

			BasicString subString(const BasicInt&) const;
			BasicString subString(const BasicInt&, const BasicInt&) const;

			BasicString subStr(const BasicInt&) const;
			BasicString subStr(const BasicInt&, const BasicInt&) const;

			BasicInt indexOf(const BasicString&) const;
			BasicInt indexOf(const BasicString&, const BasicInt&) const;
			BasicInt indexOf(const BasicString&, const BasicInt&, const BasicInt&) const;

			BasicInt lastIndexOf(const BasicString&) const;
			BasicInt lastIndexOf(const BasicString&, const BasicInt&) const;
			BasicInt lastIndexOf(const BasicString&, const BasicInt&, const BasicInt&) const;

			BasicInt repeat(const BasicString&) const;
			BasicInt repeat(const BasicString&, const BasicInt&) const;
			BasicInt repeat(const BasicString&, const BasicInt&, const BasicInt&) const;

			BasicString rev() const;

			BasicString ins(const BasicString&) const;
			BasicString ins(const BasicString&, const BasicInt&) const;

			bool endsWith(const BasicString&) const;

			BasicString repl(const BasicString&, const BasicString&) const;
			BasicString repl(const BasicString&, const BasicString&, const BasicInt&) const;
			BasicString repl(const BasicString&, const BasicString&, const BasicInt&, const BasicInt&) const;

			BasicString rmv(const BasicString&) const;

			BasicString rmvString(const BasicInt&) const;
			BasicString rmvString(const BasicInt&, const BasicInt&) const;

			BasicString rmvStr(const BasicInt&) const;
			BasicString rmvStr(const BasicInt&, const BasicInt&) const;

			bool startsWith(const BasicString&) const;

			BasicString toLower() const;

			BasicString toUpper() const;

			BasicString trim() const;

			BasicString operator+(const BasicString&) const;
			BasicString operator-(const BasicString&) const;
			BasicString operator*(const BasicInt&) const;

			BasicString operator+(const BasicChar&) const;
			BasicString operator-(const BasicChar&) const;

			BasicString& operator+=(const BasicString&) const;
			BasicString& operator-=(const BasicString&) const;
			BasicString& operator*=(const BasicInt&) const;

			BasicString& operator+=(const BasicChar&) const;
			BasicString& operator-=(const BasicChar&) const;

			BasicString& operator=(const char*) const;
			BasicString& operator=(const std::string&) const;
			BasicString& operator=(const BasicString&) const;
			BasicString& operator=(const BasicChar&) const;

			bool operator==(const BasicString&) const;
			bool operator!=(const BasicString&) const;

			bool operator>(const BasicString&) const;
			bool operator>(const BasicInt&) const;
			bool operator<(const BasicString&) const;
			bool operator<(const BasicInt&) const;
			bool operator>=(const BasicString&) const;
			bool operator>=(const BasicInt&) const;
			bool operator<=(const BasicString&) const;
			bool operator<=(const BasicInt&) const;

			char& operator[](const int&) const;

			operator char* () const;
			operator std::string() const;

		private:
			mutable BasicInt l = -1;
			mutable char* v;
		};

		class BasicFloat
		{
		public:
			BasicFloat();
			BasicFloat(const float&);
			BasicFloat(const BasicFloat&);

			float& val() const;
			BasicFloat& val(const float&) const;
			BasicFloat& val(const BasicFloat&) const;

			BasicInt toCeil() const;

			BasicInt toFloor() const;

			BasicInt toRound() const;

			BasicFloat& operator+=(const BasicFloat&) const;
			BasicFloat& operator-=(const BasicFloat&) const;
			BasicFloat& operator*=(const BasicFloat&) const;
			BasicFloat& operator/=(const BasicFloat&) const;

			BasicFloat& operator=(const float&) const;
			BasicFloat& operator=(const BasicFloat&) const;

			operator float() const;

			Type type() const;

		private:
			mutable float v;
		};

		class BasicDouble
		{
		public:
			BasicDouble();
			BasicDouble(const double&);
			BasicDouble(const BasicDouble&);

			double& val() const;
			BasicDouble& val(const double&) const;
			BasicDouble& val(const BasicDouble&) const;

			BasicInt toCeil() const;

			BasicInt toFloor() const;

			BasicInt toRound() const;

			BasicDouble& operator+=(const BasicDouble&) const;
			BasicDouble& operator-=(const BasicDouble&) const;
			BasicDouble& operator*=(const BasicDouble&) const;
			BasicDouble& operator/=(const BasicDouble&) const;

			BasicDouble& operator=(const double&) const;
			BasicDouble& operator=(const BasicDouble&) const;

			operator double() const;

			Type type() const;

		private:
			mutable double v;
		};

		class BasicLongDouble
		{
		public:
			BasicLongDouble();
			BasicLongDouble(const long double&);
			BasicLongDouble(const BasicLongDouble&);

			long double& val() const;
			BasicLongDouble& val(const long double&) const;
			BasicLongDouble& val(const BasicLongDouble&) const;

			BasicInt toCeil() const;

			BasicInt toFloor() const;

			BasicInt toRound() const;

			BasicLongDouble& operator+=(const BasicLongDouble&) const;
			BasicLongDouble& operator-=(const BasicLongDouble&) const;
			BasicLongDouble& operator*=(const BasicLongDouble&) const;
			BasicLongDouble& operator/=(const BasicLongDouble&) const;

			BasicLongDouble& operator=(const long double&) const;
			BasicLongDouble& operator=(const BasicLongDouble&) const;

			operator long double() const;

			Type type() const;

		private:
			mutable long double v;
		};

	private:
		template<class I>
		I intern_toNum(BasicString);

		template<class I>
		BasicString intern_base(I, int);

		int intern_val(char);
	};

	typedef Var::BasicInt Int;
	typedef Var::BasicLong Long;
	typedef Var::BasicUnsigned Unsigned;
	typedef Var::BasicUnsignedLong UnsignedLong;
	typedef Var::BasicShort Short;
	typedef Var::BasicUnsignedShort UnsignedShort;
	typedef Var::BasicChar Char;
	typedef Var::BasicString String;
	typedef Var::BasicFloat Float;
	typedef Var::BasicDouble Double;
	typedef Var::BasicLongDouble LongDouble;

	class Array
	{
	private:
		class ArrayDef
		{
		private:
			union ArrayTypes
			{
			public:
				mutable Int IntV;
				mutable Long LongV;
				mutable Unsigned UnsignedV;
				mutable UnsignedLong UnsignedLongV;
				mutable Short ShortV;
				mutable UnsignedShort UnsignedShortV;
				mutable Char CharV;
				mutable String StringV;
				mutable Float FloatV;
				mutable Double DoubleV;
				mutable LongDouble LongDoubleV;
				mutable bool nullV;

				ArrayTypes();
				ArrayTypes(const Int&);
				ArrayTypes(const Long&);
				ArrayTypes(const Unsigned&);
				ArrayTypes(const UnsignedLong&);
				ArrayTypes(const Short&);
				ArrayTypes(const UnsignedShort&);
				ArrayTypes(const Char&);
				ArrayTypes(const String&);
				ArrayTypes(const Float&);
				ArrayTypes(const Double&);
				ArrayTypes(const LongDouble&);

				ArrayTypes(const int&);
				ArrayTypes(const long&);
				ArrayTypes(const unsigned&);
				ArrayTypes(const unsigned long&);
				ArrayTypes(const short&);
				ArrayTypes(const unsigned short&);
				ArrayTypes(const char&);
				ArrayTypes(const char*);
				ArrayTypes(const float&);
				ArrayTypes(const double&);
				ArrayTypes(const long double&);

				bool equalsTo(const ArrayTypes&, const Type&) const;

				~ArrayTypes();
			};

			mutable bool nullStatus = true;
			mutable Type tp;
			mutable ArrayTypes v;

		public:
			ArrayDef();
			ArrayDef(const ArrayDef&);
			ArrayDef(const Int&);
			ArrayDef(const Long&);
			ArrayDef(const Unsigned&);
			ArrayDef(const UnsignedLong&);
			ArrayDef(const Short&);
			ArrayDef(const UnsignedShort&);
			ArrayDef(const Char&);
			ArrayDef(const String&);
			ArrayDef(const Float&);
			ArrayDef(const Double&);
			ArrayDef(const LongDouble&);

			ArrayDef(const int&);
			ArrayDef(const long&);
			ArrayDef(const unsigned&);
			ArrayDef(const unsigned long&);
			ArrayDef(const short&);
			ArrayDef(const unsigned short&);
			ArrayDef(const char&);
			ArrayDef(const char*);
			ArrayDef(const float&);
			ArrayDef(const double&);
			ArrayDef(const long double&);

			Type getType() const;
			Int getInt() const;
			Long getLong() const;
			Unsigned getUnsigned() const;
			UnsignedLong getUnsignedLong() const;
			Short getShort() const;
			UnsignedShort getUnsignedShort() const;
			Char getChar() const;
			String getString() const;
			Float getFloat() const;
			Double getDouble() const;
			LongDouble getLongDouble() const;

			bool isNull() const;
			ArrayDef& clear() const;

			bool equalsTo(const ArrayDef&) const;

			operator Int() const;
			operator Long() const;
			operator Unsigned() const;
			operator UnsignedLong() const;
			operator Short() const;
			operator UnsignedShort() const;
			operator Char() const;
			operator String() const;
			operator Float() const;
			operator Double() const;
			operator LongDouble() const;

			operator int() const;
			operator long() const;
			operator unsigned() const;
			operator unsigned long() const;
			operator short() const;
			operator unsigned short() const;
			operator char() const;
			operator const char* () const;
			operator float() const;
			operator double() const;
			operator long double() const;

			template<class T>
			operator T() const;

			void operator=(const ArrayDef&) const;
		};

		mutable ArrayDef* v;
		mutable Int l = 0;

	public:
		Array();
		Array(const ArrayDef&);
		Array(const Array&);

		Int& len() const;

		Array& append(const Array&) const;
		Array& append(const ArrayDef&) const;
		template<class... T>
		Array& append(const Array&, const T&...) const;
		template<class... T>
		Array& append(const ArrayDef&, const T&...) const;

		Array copyWithin(const Int&) const;
		Array copyWithin(const Int&, const Int&) const;
		Array copyWithin(const Int&, const Int&, const Int&) const;

		Int indexOf(const Array&) const;
		Int indexOf(const ArrayDef&) const;
		Int indexOf(const Array&, const Int&) const;
		Int indexOf(const ArrayDef&, const Int&) const;
		Int indexOf(const Array&, const Int&, const Int&) const;
		Int indexOf(const ArrayDef&, const Int&, const Int&) const;

		Array ins(const Array&) const;
		Array ins(const ArrayDef&) const;
		Array ins(const Array&, const Int&) const;
		Array ins(const ArrayDef&, const Int&) const;

		Int lastIndexOf(const Array&) const;
		Int lastIndexOf(const ArrayDef&) const;
		Int lastIndexOf(const Array&, const Int&) const;
		Int lastIndexOf(const ArrayDef&, const Int&) const;
		Int lastIndexOf(const Array&, const Int&, const Int&) const;
		Int lastIndexOf(const ArrayDef&, const Int&, const Int&) const;

		Array rmv(const Array&) const;
		Array rmv(const ArrayDef&) const;

		Array rmvArr(const Int&) const;
		Array rmvArr(const Int&, const Int&) const;

		Array rmvArray(const Int&) const;
		Array rmvArray(const Int&, const Int&) const;

		Array repl(const Array&, const Array&) const;
		Array repl(const ArrayDef&, const Array&) const;
		Array repl(const Array&, const ArrayDef&) const;
		Array repl(const ArrayDef&, const ArrayDef&) const;
		Array repl(const Array&, const Array&, const Int&) const;
		Array repl(const ArrayDef&, const Array&, const Int&) const;
		Array repl(const Array&, const ArrayDef&, const Int&) const;
		Array repl(const ArrayDef&, const ArrayDef&, const Int&) const;
		Array repl(const Array&, const Array&, const Int&, const Int&) const;
		Array repl(const ArrayDef&, const Array&, const Int&, const Int&) const;
		Array repl(const Array&, const ArrayDef&, const Int&, const Int&) const;
		Array repl(const ArrayDef&, const ArrayDef&, const Int&, const Int&) const;

		Array subArr(const Int&) const;
		Array subArr(const Int&, const Int&) const;

		Array subArray(const Int&) const;
		Array subArray(const Int&, const Int&) const;

		Array operator+(const Array&) const;
		Array operator+(const ArrayDef&) const;
		Array operator*(const Int&) const;

		Array& operator+=(const Array&) const;
		Array& operator+=(const ArrayDef&) const;
		Array& operator*=(const Int&) const;

		Array& operator=(const Array&) const;
		Array& operator=(const ArrayDef&) const;

		bool operator==(const Array&) const;
		bool operator==(const ArrayDef&) const;
		bool operator!=(const Array&) const;
		bool operator!=(const ArrayDef&) const;

		bool operator>(const Array&) const;
		bool operator>(const ArrayDef&) const;
		bool operator>(const Int&) const;
		bool operator<(const Array&) const;
		bool operator<(const ArrayDef&) const;
		bool operator<(const Int&) const;
		bool operator>=(const Array&) const;
		bool operator>=(const ArrayDef&) const;
		bool operator>=(const Int&) const;
		bool operator<=(const Array&) const;
		bool operator<=(const ArrayDef&) const;
		bool operator<=(const Int&) const;

		ArrayDef& operator[](const int&) const;
	};
} // namespace corv

/*

***********
*Int class*
***********

*/

corv::Var::BasicInt::BasicInt()
{
	v = 0;
}
corv::Var::BasicInt::BasicInt(const int& value)
{
	v = value;
}
corv::Var::BasicInt::BasicInt(const BasicInt& value)
{
	v = value.val();
}
corv::Var::BasicInt::BasicInt(const int& value, const int& b)
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
}

int& corv::Var::BasicInt::val() const
{
	return (int&)v;
}
corv::Var::BasicInt& corv::Var::BasicInt::val(const int& value) const
{
	v = value;
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::val(const BasicInt& value) const
{
	v = value.val();
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::val(const int& value, const int& b) const
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
	return (BasicInt&)* this;
}

corv::Var::BasicInt corv::Var::BasicInt::absolute() const
{
	return -v;
}

corv::Var::BasicInt corv::Var::BasicInt::toBase(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 10)
		value = 10;
	Var obj;
	return obj.intern_toNum<int>(obj.intern_base(v, value));
}
corv::Var::BasicString corv::Var::BasicInt::toBaseString(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 36)
		value = 36;
	Var obj;
	return obj.intern_base(v, value);
}

corv::Var::BasicInt corv::Var::BasicInt::pow() const
{
	return v * v;
}
corv::Var::BasicInt corv::Var::BasicInt::pow(const BasicInt& value) const
{
	BasicInt res = 1;
	for (int i = 0; i < value.val(); i++) {
		res *= v;
	}
	return res;
}

corv::Var::BasicInt& corv::Var::BasicInt::operator+=(const BasicInt& value) const
{
	v += value.val();
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator-=(const BasicInt& value) const
{
	v -= value.val();
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator*=(const BasicInt& value) const
{
	v *= value.val();
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator/=(const BasicInt& value) const
{
	v /= value.val();
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator%=(const BasicInt& value) const
{
	v %= value.val();
	return (BasicInt&)* this;
}

corv::Var::BasicInt& corv::Var::BasicInt::operator++() const
{
	v++;
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator--() const
{
	v--;
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator++(int) const
{
	++v;
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator--(int) const
{
	--v;
	return (BasicInt&)* this;
}

corv::Var::BasicInt& corv::Var::BasicInt::operator=(const int& value) const
{
	v = value;
	return (BasicInt&)* this;
}
corv::Var::BasicInt& corv::Var::BasicInt::operator=(const BasicInt& value) const
{
	v = value.val();
	return (BasicInt&)* this;
}

corv::Var::BasicInt::operator int() const
{
	return v;
}

corv::Type corv::Var::BasicInt::type() const
{
	return Type::Int;
}

/*

************
*Long class*
************

*/

corv::Var::BasicLong::BasicLong()
{
	v = 0;
}
corv::Var::BasicLong::BasicLong(const long& value)
{
	v = value;
}
corv::Var::BasicLong::BasicLong(const BasicLong& value)
{
	v = value.val();
}
corv::Var::BasicLong::BasicLong(const long& value, const int& b)
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
}

long& corv::Var::BasicLong::val() const
{
	return (long&)v;
}
corv::Var::BasicLong& corv::Var::BasicLong::val(const long& value) const
{
	v = value;
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::val(const BasicLong& value) const
{
	v = value.val();
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::val(const long& value, const int& b) const
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
	return (BasicLong&)* this;
}

corv::Var::BasicLong corv::Var::BasicLong::absolute() const
{
	return -v;
}

corv::Var::BasicLong corv::Var::BasicLong::toBase(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 10)
		value = 10;
	Var obj;
	return obj.intern_toNum<int>(obj.intern_base(v, value));
}
corv::Var::BasicString corv::Var::BasicLong::toBaseString(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 36)
		value = 36;
	Var obj;
	return obj.intern_base(v, value);
}

corv::Var::BasicLong corv::Var::BasicLong::pow() const
{
	return v * v;
}
corv::Var::BasicLong corv::Var::BasicLong::pow(const BasicLong& value) const
{
	BasicLong res = 1;
	for (int i = 0; i < value.val(); i++) {
		res *= v;
	}
	return res;
}

corv::Var::BasicLong& corv::Var::BasicLong::operator+=(const BasicLong& value) const
{
	v += value.val();
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator-=(const BasicLong& value) const
{
	v -= value.val();
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator*=(const BasicLong& value) const
{
	v *= value.val();
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator/=(const BasicLong& value) const
{
	v /= value.val();
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator%=(const BasicLong& value) const
{
	v %= value.val();
	return (BasicLong&)* this;
}

corv::Var::BasicLong& corv::Var::BasicLong::operator++() const
{
	v++;
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator--() const
{
	v--;
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator++(int) const
{
	++v;
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator--(int) const
{
	--v;
	return (BasicLong&)* this;
}

corv::Var::BasicLong& corv::Var::BasicLong::operator=(const long& value) const
{
	v = value;
	return (BasicLong&)* this;
}
corv::Var::BasicLong& corv::Var::BasicLong::operator=(const BasicLong& value) const
{
	v = value.val();
	return (BasicLong&)* this;
}

corv::Var::BasicLong::operator long() const
{
	return v;
}

corv::Type corv::Var::BasicLong::type() const
{
	return Type::Long;
}

/*

****************
*Unsigned class*
****************

*/

corv::Var::BasicUnsigned::BasicUnsigned()
{
	v = 0;
}
corv::Var::BasicUnsigned::BasicUnsigned(const unsigned& value)
{
	v = value;
}
corv::Var::BasicUnsigned::BasicUnsigned(const BasicUnsigned& value)
{
	v = value.val();
}
corv::Var::BasicUnsigned::BasicUnsigned(const unsigned& value, const int& b)
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
}

unsigned& corv::Var::BasicUnsigned::val() const
{
	return (unsigned&)v;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::val(const unsigned& value) const
{
	v = value;
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::val(const BasicUnsigned& value) const
{
	v = value.val();
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::val(const unsigned& value, const int& b) const
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
	return (BasicUnsigned&)* this;
}

corv::Var::BasicUnsigned corv::Var::BasicUnsigned::absolute() const
{
	return -v;
}

corv::Var::BasicUnsigned corv::Var::BasicUnsigned::toBase(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 10)
		value = 10;
	Var obj;
	return obj.intern_toNum<int>(obj.intern_base(v, value));
}
corv::Var::BasicString corv::Var::BasicUnsigned::toBaseString(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 36)
		value = 36;
	Var obj;
	return obj.intern_base(v, value);
}

corv::Var::BasicUnsigned corv::Var::BasicUnsigned::pow() const
{
	return v * v;
}
corv::Var::BasicUnsigned corv::Var::BasicUnsigned::pow(const BasicUnsigned& value) const
{
	BasicUnsigned res = 1;
	for (int i = 0; i < value.val(); i++) {
		res *= v;
	}
	return res;
}

corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator+=(const BasicUnsigned& value) const
{
	v += value.val();
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator-=(const BasicUnsigned& value) const
{
	v -= value.val();
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator*=(const BasicUnsigned& value) const
{
	v *= value.val();
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator/=(const BasicUnsigned& value) const
{
	v /= value.val();
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator%=(const BasicUnsigned& value) const
{
	v %= value.val();
	return (BasicUnsigned&)* this;
}

corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator++() const
{
	v++;
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator--() const
{
	v--;
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator++(int) const
{
	++v;
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator--(int) const
{
	--v;
	return (BasicUnsigned&)* this;
}

corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator=(const unsigned& value) const
{
	v = value;
	return (BasicUnsigned&)* this;
}
corv::Var::BasicUnsigned& corv::Var::BasicUnsigned::operator=(const BasicUnsigned& value) const
{
	v = value.val();
	return (BasicUnsigned&)* this;
}

corv::Var::BasicUnsigned::operator unsigned() const
{
	return v;
}

corv::Type corv::Var::BasicUnsigned::type() const
{
	return Type::Unsigned;
}

/*

********************
*UnsignedLong class*
********************

*/

corv::Var::BasicUnsignedLong::BasicUnsignedLong()
{
	v = 0;
}
corv::Var::BasicUnsignedLong::BasicUnsignedLong(const unsigned long& value)
{
	v = value;
}
corv::Var::BasicUnsignedLong::BasicUnsignedLong(const BasicUnsignedLong& value)
{
	v = value.val();
}
corv::Var::BasicUnsignedLong::BasicUnsignedLong(const unsigned long& value, const int& b)
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
}

unsigned long& corv::Var::BasicUnsignedLong::val() const
{
	return (unsigned long&)v;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::val(const unsigned long& value) const
{
	v = value;
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::val(const BasicUnsignedLong& value) const
{
	v = value.val();
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::val(const unsigned long& value, const int& b) const
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
	return (BasicUnsignedLong&)* this;
}

corv::Var::BasicUnsignedLong corv::Var::BasicUnsignedLong::absolute() const
{
	return -v;
}

corv::Var::BasicUnsignedLong corv::Var::BasicUnsignedLong::toBase(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 10)
		value = 10;
	Var obj;
	return obj.intern_toNum<int>(obj.intern_base(v, value));
}
corv::Var::BasicString corv::Var::BasicUnsignedLong::toBaseString(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 36)
		value = 36;
	Var obj;
	return obj.intern_base(v, value);
}

corv::Var::BasicUnsignedLong corv::Var::BasicUnsignedLong::pow() const
{
	return v * v;
}
corv::Var::BasicUnsignedLong corv::Var::BasicUnsignedLong::pow(const BasicUnsignedLong& value) const
{
	BasicUnsignedLong res = 1;
	for (int i = 0; i < value.val(); i++) {
		res *= v;
	}
	return res;
}

corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator+=(const BasicUnsignedLong& value) const
{
	v += value.val();
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator-=(const BasicUnsignedLong& value) const
{
	v -= value.val();
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator*=(const BasicUnsignedLong& value) const
{
	v *= value.val();
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator/=(const BasicUnsignedLong& value) const
{
	v /= value.val();
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator%=(const BasicUnsignedLong& value) const
{
	v %= value.val();
	return (BasicUnsignedLong&)* this;
}

corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator++() const
{
	v++;
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator--() const
{
	v--;
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator++(int) const
{
	++v;
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator--(int) const
{
	--v;
	return (BasicUnsignedLong&)* this;
}

corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator=(const unsigned long& value) const
{
	v = value;
	return (BasicUnsignedLong&)* this;
}
corv::Var::BasicUnsignedLong& corv::Var::BasicUnsignedLong::operator=(const BasicUnsignedLong& value) const
{
	v = value.val();
	return (BasicUnsignedLong&)* this;
}

corv::Var::BasicUnsignedLong::operator unsigned long() const
{
	return v;
}

corv::Type corv::Var::BasicUnsignedLong::type() const
{
	return Type::UnsignedLong;
}

/*

*************
*Short class*
*************

*/

corv::Var::BasicShort::BasicShort()
{
	v = 0;
}
corv::Var::BasicShort::BasicShort(const short& value)
{
	v = value;
}
corv::Var::BasicShort::BasicShort(const BasicShort& value)
{
	v = value.val();
}
corv::Var::BasicShort::BasicShort(const short& value, const int& b)
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
}

short& corv::Var::BasicShort::val() const
{
	return (short&)v;
}
corv::Var::BasicShort& corv::Var::BasicShort::val(const short& value) const
{
	v = value;
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::val(const BasicShort& value) const
{
	v = value.val();
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::val(const short& value, const int& b) const
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
	return (BasicShort&)* this;
}

corv::Var::BasicShort corv::Var::BasicShort::absolute() const
{
	return -v;
}

corv::Var::BasicShort corv::Var::BasicShort::toBase(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 10)
		value = 10;
	Var obj;
	return obj.intern_toNum<int>(obj.intern_base(v, value));
}
corv::Var::BasicString corv::Var::BasicShort::toBaseString(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 36)
		value = 36;
	Var obj;
	return obj.intern_base(v, value);
}

corv::Var::BasicShort corv::Var::BasicShort::pow() const
{
	return v * v;
}
corv::Var::BasicShort corv::Var::BasicShort::pow(const BasicShort& value) const
{
	BasicShort res = 1;
	for (int i = 0; i < value.val(); i++) {
		res *= v;
	}
	return res;
}

corv::Var::BasicShort& corv::Var::BasicShort::operator+=(const BasicShort& value) const
{
	v += value.val();
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator-=(const BasicShort& value) const
{
	v -= value.val();
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator*=(const BasicShort& value) const
{
	v *= value.val();
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator/=(const BasicShort& value) const
{
	v /= value.val();
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator%=(const BasicShort& value) const
{
	v %= value.val();
	return (BasicShort&)* this;
}

corv::Var::BasicShort& corv::Var::BasicShort::operator++() const
{
	v++;
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator--() const
{
	v--;
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator++(int) const
{
	++v;
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator--(int) const
{
	--v;
	return (BasicShort&)* this;
}

corv::Var::BasicShort& corv::Var::BasicShort::operator=(const short& value) const
{
	v = value;
	return (BasicShort&)* this;
}
corv::Var::BasicShort& corv::Var::BasicShort::operator=(const BasicShort& value) const
{
	v = value.val();
	return (BasicShort&)* this;
}

corv::Var::BasicShort::operator short() const
{
	return v;
}

corv::Type corv::Var::BasicShort::type() const
{
	return Type::Short;
}

/*

*********************
*UnsignedShort class*
*********************

*/

corv::Var::BasicUnsignedShort::BasicUnsignedShort()
{
	v = 0;
}
corv::Var::BasicUnsignedShort::BasicUnsignedShort(const unsigned short& value)
{
	v = value;
}
corv::Var::BasicUnsignedShort::BasicUnsignedShort(const BasicUnsignedShort& value)
{
	v = value.val();
}
corv::Var::BasicUnsignedShort::BasicUnsignedShort(const unsigned short& value, const int& b)
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
}

unsigned short& corv::Var::BasicUnsignedShort::val() const
{
	return (unsigned short&)v;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::val(const unsigned short& value) const
{
	v = value;
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::val(const BasicUnsignedShort& value) const
{
	v = value.val();
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::val(const unsigned short& value, const int& b) const
{
	Var obj;
	BasicString strV = obj.intern_base(value, b);
	int power = 1;
	int n = 0;
	for (int i = strV.len() - 1; i >= 0; i--) {
		if (obj.intern_val(strV[i]) >= b) {
			n = 0;
			break;
		}
		n += obj.intern_val(strV[i]) * power;
		power = power * b;
	}
	v = n;
	return (BasicUnsignedShort&)* this;
}

corv::Var::BasicUnsignedShort corv::Var::BasicUnsignedShort::absolute() const
{
	return -v;
}

corv::Var::BasicUnsignedShort corv::Var::BasicUnsignedShort::toBase(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 10)
		value = 10;
	Var obj;
	return obj.intern_toNum<int>(obj.intern_base(v, value));
}
corv::Var::BasicString corv::Var::BasicUnsignedShort::toBaseString(const BasicInt& value) const
{
	if (value < 2)
		value = 2;
	if (value > 36)
		value = 36;
	Var obj;
	return obj.intern_base(v, value);
}

corv::Var::BasicUnsignedShort corv::Var::BasicUnsignedShort::pow() const
{
	return v * v;
}
corv::Var::BasicUnsignedShort corv::Var::BasicUnsignedShort::pow(const BasicUnsignedShort& value) const
{
	BasicUnsignedShort res = 1;
	for (int i = 0; i < value.val(); i++) {
		res *= v;
	}
	return res;
}

corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator+=(const BasicUnsignedShort& value) const
{
	v += value.val();
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator-=(const BasicUnsignedShort& value) const
{
	v -= value.val();
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator*=(const BasicUnsignedShort& value) const
{
	v *= value.val();
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator/=(const BasicUnsignedShort& value) const
{
	v /= value.val();
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator%=(const BasicUnsignedShort& value) const
{
	v %= value.val();
	return (BasicUnsignedShort&)* this;
}

corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator++() const
{
	v++;
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator--() const
{
	v--;
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator++(int) const
{
	++v;
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator--(int) const
{
	--v;
	return (BasicUnsignedShort&)* this;
}

corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator=(const unsigned short& value) const
{
	v = value;
	return (BasicUnsignedShort&)* this;
}
corv::Var::BasicUnsignedShort& corv::Var::BasicUnsignedShort::operator=(const BasicUnsignedShort& value) const
{
	v = value.val();
	return (BasicUnsignedShort&)* this;
}

corv::Var::BasicUnsignedShort::operator unsigned short() const
{
	return v;
}

corv::Type corv::Var::BasicUnsignedShort::type() const
{
	return Type::UnsignedShort;
}

/*

************
*Char class*
************

*/

corv::Var::BasicChar::BasicChar()
{
	v = '\0';
}
corv::Var::BasicChar::BasicChar(const char& value)
{
	v = value;
}
corv::Var::BasicChar::BasicChar(const BasicChar& value)
{
	v = value.val();
}
corv::Var::BasicChar::BasicChar(const BasicInt& value)
{
	v = value.val();
}

char& corv::Var::BasicChar::val() const
{
	return (char&)v;
}
corv::Var::BasicChar& corv::Var::BasicChar::val(const char& value) const
{
	v = value;
	return (BasicChar&)* this;
}
corv::Var::BasicChar& corv::Var::BasicChar::val(const BasicChar& value) const
{
	v = value.val();
	return (BasicChar&)* this;
}
corv::Var::BasicChar& corv::Var::BasicChar::val(const BasicInt& value) const
{
	v = (char)value;
	return (BasicChar&)* this;
}

bool corv::Var::BasicChar::isAlnum() const
{
	if ((v >= 48 && v <= 57) || (v >= 65 && v <= 90) || (v >= 97 && v <= 122))
		return true;
	else
		return false;
}

bool corv::Var::BasicChar::isAlpha() const
{
	if ((v >= 65 && v <= 90) || (v >= 97 && v <= 122))
		return true;
	else
		return false;
}

bool corv::Var::BasicChar::isDigit() const
{
	if (v >= 48 && v <= 57)
		return true;
	else
		return false;
}

bool corv::Var::BasicChar::isLower() const
{
	if (v >= 97 && v <= 122)
		return true;
	else
		return false;
}

bool corv::Var::BasicChar::isPunct() const
{
	return !isAlnum();
}

bool corv::Var::BasicChar::isSpace() const
{
	if (v == ' ')
		return true;
	else
		return false;
}

bool corv::Var::BasicChar::isUpper() const
{
	if (v >= 65 && v <= 90)
		return true;
	else
		return false;
}

corv::Var::BasicChar corv::Var::BasicChar::toLower() const
{
	if (v >= 97 && v <= 122)
		return v - 32;
	else
		return v;
}

corv::Var::BasicChar corv::Var::BasicChar::toUpper() const
{
	if (v >= 65 && v <= 90)
		return v + 32;
	else
		return v;
}

corv::Var::BasicChar& corv::Var::BasicChar::operator=(const char& val) const
{
	v = val;
	return (BasicChar&)* this;
}
corv::Var::BasicChar& corv::Var::BasicChar::operator=(const BasicChar& val) const
{
	v = val.val();
	return (BasicChar&)* this;
}
corv::Var::BasicChar& corv::Var::BasicChar::operator=(const BasicInt& val) const
{
	v = val.val();
	return (BasicChar&)* this;
}

corv::Var::BasicChar corv::Var::BasicChar::operator+(const BasicChar& value) const
{
	return v + value.val();
}

corv::Var::BasicChar corv::Var::BasicChar::operator-(const BasicChar& value) const
{
	return v - value.val();
}

corv::Var::BasicChar corv::Var::BasicChar::operator*(const BasicChar& value) const
{
	return v * value.val();
}

corv::Var::BasicChar corv::Var::BasicChar::operator/(const BasicChar& value) const
{
	return v / value.val();
}

corv::Var::BasicChar& corv::Var::BasicChar::operator+=(const BasicChar& value) const
{
	v += value.val();
	return (BasicChar&)* this;
}

corv::Var::BasicChar& corv::Var::BasicChar::operator-=(const BasicChar& value) const
{
	v -= value.val();
	return (BasicChar&)* this;
}

corv::Var::BasicChar& corv::Var::BasicChar::operator*=(const BasicChar& value) const
{
	v *= value.val();
	return (BasicChar&)* this;
}

corv::Var::BasicChar& corv::Var::BasicChar::operator/=(const BasicChar& value) const
{
	v /= value.val();
	return (BasicChar&)* this;
}

bool corv::Var::BasicChar::operator<(const BasicChar& value) const
{
	return v < value.val();
}

bool corv::Var::BasicChar::operator>(const BasicChar& value) const
{
	return v > value.val();
}

bool corv::Var::BasicChar::operator<=(const BasicChar& value) const
{
	return v <= value.val();
}

bool corv::Var::BasicChar::operator>=(const BasicChar& value) const
{
	return v >= value.val();
}

corv::Var::BasicChar::operator char() const
{
	return v;
}

corv::Var::BasicChar::operator int() const
{
	return (int)v;
}

corv::Type corv::Var::BasicChar::type() const
{
	return Type::Char;
}

/*

**************
*String class*
**************

*/
corv::Var::BasicString::BasicString()
{
	l++;
	v = new char[l + 1];
	v[l] = '\0';
}
corv::Var::BasicString::BasicString(const char* value)
{
	int n = 0;
	while (value[n])
		n++;
	l = n;
	v = new char[l + 1];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	v[l] = '\0';
}
corv::Var::BasicString::BasicString(const std::string& value)
{
	l = value.size();
	v = new char[l + 1];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	v[l] = '\0';
}
corv::Var::BasicString::BasicString(const BasicString& value)
{
	l = value.len();
	v = new char[l + 1];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	v[l] = '\0';
}

corv::Var::BasicString::BasicString(const char* value, const BasicInt& f)
{
	int n = 0;
	while (value[n])
		n++;
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= n)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = n - from;
		v = new char[l + 1];
		for (int i = from; i < n; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
}
corv::Var::BasicString::BasicString(const std::string& value, const BasicInt& f)
{
	int n = value.size();
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= n)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = n - from;
		v = new char[l + 1];
		for (int i = from; i < n; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
}
corv::Var::BasicString::BasicString(const BasicString& value, const BasicInt& f)
{
	int n = value.len();
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= n)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = n - from;
		v = new char[l + 1];
		for (int i = from; i < n; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
}

corv::Var::BasicString::BasicString(const char* value, const BasicInt& f, const BasicInt& t)
{
	int n = 0;
	while (value[n])
		n++;
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > n)
		to = n;
	if (from >= n || from >= to || to <= 0)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = to - from;
		v = new char[l + 1];
		for (int i = from; i < to; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
}
corv::Var::BasicString::BasicString(const std::string& value, const BasicInt& f, const BasicInt& t)
{
	int n = value.size();
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > n)
		to = n;
	if (from >= n || from >= to || to <= 0)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = to - from;
		v = new char[l + 1];
		for (int i = from; i < to; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
}
corv::Var::BasicString::BasicString(const BasicString& value, const BasicInt& f, const BasicInt& t)
{
	int n = value.len();
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > n)
		to = n;
	if (from >= n || from >= to || to <= 0)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = to - from;
		v = new char[l + 1];
		for (int i = from; i < to; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
}

corv::Var::BasicString::BasicString(const BasicChar& value)
{
	l = 1;
	v = new char[l + 1];
	v[0] = value;
	v[l] = '\0';
}
corv::Var::BasicString::~BasicString()
{
	delete[] v;
}

corv::Type corv::Var::BasicString::type() const
{
	return Type::String;
}

char*& corv::Var::BasicString::val() const
{
	return (char*&)v;
}
corv::Var::BasicString& corv::Var::BasicString::val(const BasicString& value) const
{
	delete[] v;
	l = value.len();
	v = new char[l + 1];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	v[l] = '\0';
	return (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::val(const BasicString& value, const BasicInt& f) const
{
	delete[] v;
	int n = value.len();
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= n)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = n - from;
		v = new char[l + 1];
		for (int i = from; i < n; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
	return (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::val(const BasicString& value, const BasicInt& f, const BasicInt& t) const
{
	delete[] v;
	int n = value.len();
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > n)
		to = n;
	if (from >= n || from >= to || to <= 0)
	{
		l = 0;
		v = new char[l + 1];
		v[l] = '\0';
	}
	else
	{
		l = to - from;
		v = new char[l + 1];
		for (int i = from; i < to; i++)
			v[i - from] = value[i];
		v[l] = '\0';
	}
	return (BasicString&)* this;
}

corv::Var::BasicInt& corv::Var::BasicString::len() const
{
	return (BasicInt&)l;
}

corv::Var::BasicString corv::Var::BasicString::subString(const BasicInt& f) const
{
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= l)
		return BasicString("");
	else
	{
		char* res = new char[l - from + 1];
		for (int i = from; i < l; i++)
			res[i] = v[i];
		res[l - from] = '\0';
		return BasicString(res);
	}
}
corv::Var::BasicString corv::Var::BasicString::subString(const BasicInt& f, const BasicInt& t) const
{
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > l)
		to = l;
	if (from >= l || from >= to || to <= 0)
		return BasicString("");
	else
	{
		char* res = new char[(to - from) + 1];
		for (int i = from; i < to; i++)
			res[i - from] = v[i];
		res[to - from] = '\0';
		return BasicString(res);
	}
}

corv::Var::BasicString corv::Var::BasicString::subStr(const BasicInt& f) const
{
	return subString(f);
}
corv::Var::BasicString corv::Var::BasicString::subStr(const BasicInt& f, const BasicInt& length) const
{
	int from = f;
	if (from < 0)
		from = 0;
	int to = length;
	if (to + from > l)
		to = l - from;
	if (from >= l || to <= 0)
		return BasicString("");
	return subString(f, f + length);
}

corv::Var::BasicInt corv::Var::BasicString::indexOf(const BasicString& value) const
{
	int found = 0;
	for (int i = 0; i < l; i++)
		for (int ii = 0; ii < value.len(); ii++)
		{
			if (value[found] == v[i])
			{
				found++;
				if (found == value.len())
					return (i - value.len()) + 1;
				break;
			}
			else
				found = 0;
		}
	return -1;
}
corv::Var::BasicInt corv::Var::BasicString::indexOf(const BasicString& value, const BasicInt& from) const
{
	return subString(from).indexOf(value);
}
corv::Var::BasicInt corv::Var::BasicString::indexOf(const BasicString& value, const BasicInt& from, const BasicInt& to) const
{
	return subString(from, to).indexOf(value);
}

corv::Var::BasicInt corv::Var::BasicString::lastIndexOf(const BasicString& value) const
{
	int start = 0;
	while (true)
		if (indexOf(value, start) != -1)
			start = indexOf(value, start) + 1;
		else
			if (start)
				return -1;
			else
				return start - 1;
}
corv::Var::BasicInt corv::Var::BasicString::lastIndexOf(const BasicString& value, const BasicInt& from) const
{
	return subString(from).lastIndexOf(value);
}
corv::Var::BasicInt corv::Var::BasicString::lastIndexOf(const BasicString& value, const BasicInt& from, const BasicInt& to) const
{
	return subString(from, to).lastIndexOf(value);
}

corv::Var::BasicInt corv::Var::BasicString::repeat(const BasicString& value) const
{
	int index = 0;
	int start = 0;
	while (true)
	{
		if (indexOf(value, start) != -1)
		{
			start = indexOf(value, start);
			index++;
		}
		else
		{
			if (start)
				return 0;
			else
				return index;
		}
	}
}
corv::Var::BasicInt corv::Var::BasicString::repeat(const BasicString& value, const BasicInt& from) const
{
	return subString(from).repeat(value);
}
corv::Var::BasicInt corv::Var::BasicString::repeat(const BasicString& value, const BasicInt& from, const BasicInt& to) const
{
	return subString(from, to).repeat(value);
}

corv::Var::BasicString corv::Var::BasicString::rev() const
{
	char* res = new char[l + 1];
	for (int i = 0; i < l; i++)
		res[i] = v[l - i];
	res[l] = '\0';
	BasicString s = res;
	delete[] res;
	return s;
}

corv::Var::BasicString corv::Var::BasicString::ins(const BasicString& value) const
{
	char* res = new char[l + value.len() + 1];
	for (int i = 0; i < value.len(); i++)
		res[i] = value[i];
	for (int i = 0; i < l; i++)
		res[i + value.len()] = v[i];
	res[l + value.len()] = '\0';
	BasicString s = res;
	delete[] res;
	return s;
}
corv::Var::BasicString corv::Var::BasicString::ins(const BasicString& value, const BasicInt& pos) const
{
	BasicString start = subString(0, pos);
	BasicString end = subString(pos, l);
	char* res = new char[start.len() + value.len() + end.len() + 1];
	for (int i = 0; i < start.len(); i++)
		res[i] = start[i];
	for (int i = 0; i < value.len(); i++)
		res[i + start.len()] = value[i];
	for (int i = 0; i < end.len(); i++)
		res[i + start.len() + value.len()] = end[i];
	res[start.len() + value.len() + end.len()] = '\0';
	BasicString s = res;
	delete[] res;
	return s;
}

bool corv::Var::BasicString::endsWith(const BasicString& value) const
{
	BasicString temp = subString(l - value.len(), l);
	for (int i = 0; i < temp.len(); i++)
		if (value[i] != temp[i])
			return false;
	return true;
}

corv::Var::BasicString corv::Var::BasicString::repl(const BasicString& value, const BasicString& newValue) const
{
	int ind = 0;
	if (indexOf(value) == -1)
		return *this;
	int sl = l;
	int ol = value.len();
	int nl = newValue.len();
	bool verify = false;
	int n = 0;
	BasicString res;
	for (int i = 0; i < l; i++)
	{
		char c = v[i];
		if (verify)
		{
			if (n < ol)
			{
				if (c == value[n])
				{
					n++;
					if (i == sl - 1)
						res += newValue.val();
					continue;
				}
				else
				{
					for (int ii = n; ii > 0; ii--)
						res += v[i - ii];
					n = 0;
					verify = false;
				}
			}
			else
			{
				res += newValue;
				n = 0;
				verify = false;
			}
		}
		if (c == value[0])
		{
			if (i == sl - 1)
			{
				if (ol > 1)
					res += v[sl - 1];
				else
					res += newValue;
			}
			else
			{
				n++;
				verify = true;
				continue;
			}
		}
		else
			res += c;
		return res;
	}
}
corv::Var::BasicString corv::Var::BasicString::repl(const BasicString& value, const BasicString& newValue, const BasicInt& from) const
{
	return subString(from).repl(value, newValue);
}
corv::Var::BasicString corv::Var::BasicString::repl(const BasicString& value, const BasicString& newValue, const BasicInt& from, const BasicInt& to) const
{
	return subString(from, to).repl(value, newValue);
}

corv::Var::BasicString corv::Var::BasicString::rmv(const BasicString& value) const
{
	return repl(value, "");
}

corv::Var::BasicString corv::Var::BasicString::rmvString(const BasicInt& from) const
{
	return subString(0, from);
}
corv::Var::BasicString corv::Var::BasicString::rmvString(const BasicInt& from, const BasicInt& to) const
{
	if (to <= from)
		return *this;
	else
		return subString(0, from) + subString(to, l);
}

corv::Var::BasicString corv::Var::BasicString::rmvStr(const BasicInt& from) const
{
	return subString(0, from);
}
corv::Var::BasicString corv::Var::BasicString::rmvStr(const BasicInt& from, const BasicInt& length) const
{
	if (length > l)
		return rmvString(from);
	if (length <= 0)
		return *this;
	return rmvString(from, from + length);
}

bool corv::Var::BasicString::startsWith(const BasicString& value) const
{
	BasicString temp = subString(0, value.len());
	for (int i = 0; i < temp.len(); i++)
		if (value[i] != temp[i])
			return false;
	return true;
}

corv::Var::BasicString corv::Var::BasicString::toLower() const
{
	bool verify = false;
	BasicString res;
	int n = 0;
	BasicString upr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	BasicString lwr = "abcdefghijklmnopqrstuvwxyz";
	for (int i = 0; i < l; i++)
	{
		for (int ii = 0; ii < upr.len(); ii++)
		{
			if (v[i] = upr[ii])
			{
				verify = true;
				break;
			}
			else
				verify = false;
			n++;
		}
		if (verify)
			res += lwr[n];
		else
			res += v[i];
		i = 0;
	}
	return res;
}

corv::Var::BasicString corv::Var::BasicString::toUpper() const
{
	bool verify = false;
	BasicString res;
	int n = 0;
	BasicString upr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	BasicString lwr = "abcdefghijklmnopqrstuvwxyz";
	for (int i = 0; i < l; i++)
	{
		for (int ii = 0; ii < upr.len(); ii++)
		{
			if (v[i] = lwr[ii])
			{
				verify = true;
				break;
			}
			else
				verify = false;
			n++;
		}
		if (verify)
			res += upr[n];
		else
			res += v[i];
		i = 0;
	}
	return res;
}

corv::Var::BasicString corv::Var::BasicString::trim() const
{
	BasicString res = v;
	while (res.indexOf("  ") != -1)
		res = res.repl("  ", " ");
	return res;
}

corv::Var::BasicString corv::Var::BasicString::operator+(const BasicString& value) const
{
	char* temp = new char[l + value.len() + 1];
	for (int i = 0; i < l; i++)
		temp[i] = v[i];
	for (int i = 0; i < value.len(); i++)
		temp[i + l] = value[i];
	temp[l + value.len()] = '\0';
	BasicString s = temp;
	delete[] temp;
	return s;
}
corv::Var::BasicString corv::Var::BasicString::operator-(const BasicString& value) const
{
	if (endsWith(value))
		return subString(0, l - value.len());
	else
		return v;
}
corv::Var::BasicString corv::Var::BasicString::operator*(const BasicInt& value) const
{
	BasicString res;
	for (int i = 0; i < value.val(); i++)
		res += v;
	return res;
}

corv::Var::BasicString corv::Var::BasicString::operator+(const BasicChar& value) const
{
	return (*this) + BasicString(value);
}
corv::Var::BasicString corv::Var::BasicString::operator-(const BasicChar& value) const
{
	return (*this) - BasicString(value);
}

corv::Var::BasicString& corv::Var::BasicString::operator+=(const BasicString& value) const
{
	char* temp = new char[l + value.len() + 1];
	for (int i = 0; i < l; i++)
		temp[i] = v[i];
	for (int i = 0; i < value.len(); i++)
		temp[i + l] = value[i];
	temp[l + value.len()] = '\0';
	val(temp);
	delete[] temp;
	return  (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::operator-=(const BasicString& value) const
{
	if (endsWith(value))
		val(subString(0, l - value.len()));
	return (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::operator*=(const BasicInt& value) const
{
	l *= value;
	BasicString res;
	for (int i = 0; i < value.val(); i++)
		res += v;
	val(res);
	return (BasicString&)* this;
}

corv::Var::BasicString& corv::Var::BasicString::operator+=(const BasicChar& value) const
{
	operator+=(BasicString(value));
	return (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::operator-=(const BasicChar& value) const
{
	operator-=(BasicString(value));
	return (BasicString&)* this;
}

corv::Var::BasicString& corv::Var::BasicString::operator=(const char* value) const
{
	int n = 0;
	while (value[n])
		n++;
	l = n;
	v = new char[0];
	delete[] v;
	v = new char[l + 1];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	v[l] = '\0';
	return (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::operator=(const std::string& value) const
{
	int n = value.size();
	l = n;
	v = new char[0];
	delete[] v;
	v = new char[l + 1];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	v[l] = '\0';
	return (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::operator=(const BasicString& value) const
{
	int n = value.len();
	l = n;
	v = new char[0];
	delete[] v;
	v = new char[l + 1];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	v[l] = '\0';
	return (BasicString&)* this;
}
corv::Var::BasicString& corv::Var::BasicString::operator=(const BasicChar& value) const
{
	l = 1;
	v = new char[0];
	delete[] v;
	v = new char[l + 1];
	v[0] = value;
	v[l] = '\0';
	return (BasicString&)* this;
}

bool corv::Var::BasicString::operator==(const BasicString& value) const
{
	if (l == value.len())
	{
		for (int i = 0; i < l; i++)
			if (v[i] != value[i])
				return false;
		return true;
	}
	else
		return false;
}
bool corv::Var::BasicString::operator!=(const BasicString& value) const
{
	return !(BasicString(v) == value);
}

bool corv::Var::BasicString::operator<(const BasicString& value) const
{
	return l < value.len();
}
bool corv::Var::BasicString::operator<(const BasicInt& value) const
{
	return l < value;
}
bool corv::Var::BasicString::operator>(const BasicString& value) const
{
	return l > value.len();
}
bool corv::Var::BasicString::operator>(const BasicInt& value) const
{
	return l > value;
}
bool corv::Var::BasicString::operator<=(const BasicString& value) const
{
	return l <= value.len();
}
bool corv::Var::BasicString::operator<=(const BasicInt& value) const
{
	return l <= value;
}
bool corv::Var::BasicString::operator>=(const BasicString& value) const
{
	return l >= value.len();
}
bool corv::Var::BasicString::operator>=(const BasicInt& value) const
{
	return l >= value;
}

char& corv::Var::BasicString::operator[](const int& value) const
{
	int ind = value;
	if (ind < 0)
		ind += l;
	if (ind < 0)
		ind = 0;
	if (ind >= l)
		ind = l - 1;
	return (char&)v[ind];
}

corv::Var::BasicString::operator char* () const
{
	return v;
}
corv::Var::BasicString::operator std::string() const
{
	return v;
}

/*

*************
*Float class*
*************

*/
corv::Var::BasicFloat::BasicFloat()
{
	v = 0;
}
corv::Var::BasicFloat::BasicFloat(const float& value)
{
	v = value;
}
corv::Var::BasicFloat::BasicFloat(const BasicFloat& value)
{
	v = value.val();
}

float& corv::Var::BasicFloat::val() const
{
	return (float&)v;
}
corv::Var::BasicFloat& corv::Var::BasicFloat::val(const float& value) const
{
	v = value;
	return (BasicFloat&)* this;
}
corv::Var::BasicFloat& corv::Var::BasicFloat::val(const BasicFloat& value) const
{
	v = value.val();
	return (BasicFloat&)* this;
}

corv::Var::BasicInt corv::Var::BasicFloat::toCeil() const
{
	int temp = (int)v;
	if (temp < v)
		temp++;
	return temp;
}

corv::Var::BasicInt corv::Var::BasicFloat::toFloor() const
{
	int temp = (int)v;
	if (temp > v)
		temp++;
	return temp;
}

corv::Var::BasicInt corv::Var::BasicFloat::toRound() const
{
	return v < 0 ? int(v - 0.5) : int(v + 0.5);
}

corv::Var::BasicFloat& corv::Var::BasicFloat::operator+=(const BasicFloat& value) const
{
	v += value.val();
	return (BasicFloat&)* this;
}
corv::Var::BasicFloat& corv::Var::BasicFloat::operator-=(const BasicFloat& value) const
{
	v -= value.val();
	return (BasicFloat&)* this;
}
corv::Var::BasicFloat& corv::Var::BasicFloat::operator*=(const BasicFloat& value) const
{
	v *= value.val();
	return (BasicFloat&)* this;
}
corv::Var::BasicFloat& corv::Var::BasicFloat::operator/=(const BasicFloat& value) const
{
	v /= value.val();
	return (BasicFloat&)* this;
}

corv::Var::BasicFloat& corv::Var::BasicFloat::operator=(const float& value) const
{
	v = value;
	return (BasicFloat&)* this;
}
corv::Var::BasicFloat& corv::Var::BasicFloat::operator=(const BasicFloat& value) const
{
	v = value.val();
	return (BasicFloat&)* this;
}

corv::Var::BasicFloat::operator float() const
{
	return v;
}

corv::Type corv::Var::BasicFloat::type() const
{
	return Type::Float;
}

/*

*************
*Double class*
*************

*/

corv::Var::BasicDouble::BasicDouble()
{
	v = 0;
}
corv::Var::BasicDouble::BasicDouble(const double& value)
{
	v = value;
}
corv::Var::BasicDouble::BasicDouble(const BasicDouble& value)
{
	v = value.val();
}

double& corv::Var::BasicDouble::val() const
{
	return v;
}
corv::Var::BasicDouble& corv::Var::BasicDouble::val(const double& value) const
{
	v = value;
	return (BasicDouble&)* this;
}
corv::Var::BasicDouble& corv::Var::BasicDouble::val(const BasicDouble& value) const
{
	v = value.val();
	return (BasicDouble&)* this;
}

corv::Var::BasicInt corv::Var::BasicDouble::toCeil() const
{
	int temp = (int)v;
	if (temp < v)
		temp++;
	return temp;
}

corv::Var::BasicInt corv::Var::BasicDouble::toFloor() const
{
	int temp = (int)v;
	if (temp > v)
		temp++;
	return temp;
}

corv::Var::BasicInt corv::Var::BasicDouble::toRound() const
{
	return v < 0 ? int(v - 0.5) : int(v + 0.5);
}

corv::Var::BasicDouble& corv::Var::BasicDouble::operator+=(const BasicDouble& value) const
{
	v += value.val();
	return (BasicDouble&)* this;
}
corv::Var::BasicDouble& corv::Var::BasicDouble::operator-=(const BasicDouble& value) const
{
	v -= value.val();
	return (BasicDouble&)* this;
}
corv::Var::BasicDouble& corv::Var::BasicDouble::operator*=(const BasicDouble& value) const
{
	v *= value.val();
	return (BasicDouble&)* this;
}
corv::Var::BasicDouble& corv::Var::BasicDouble::operator/=(const BasicDouble& value) const
{
	v /= value.val();
	return (BasicDouble&)* this;
}

corv::Var::BasicDouble& corv::Var::BasicDouble::operator=(const double& value) const
{
	v = value;
	return (BasicDouble&)* this;
}
corv::Var::BasicDouble& corv::Var::BasicDouble::operator=(const BasicDouble& value) const
{
	v = value.val();
	return (BasicDouble&)* this;
}

corv::Var::BasicDouble::operator double() const
{
	return v;
}

corv::Type corv::Var::BasicDouble::type() const
{
	return Type::Double;
}

/*

******************
*LongDouble class*
******************

*/

corv::Var::BasicLongDouble::BasicLongDouble()
{
	v = 0;
}
corv::Var::BasicLongDouble::BasicLongDouble(const long double& value)
{
	v = value;
}
corv::Var::BasicLongDouble::BasicLongDouble(const BasicLongDouble& value)
{
	v = value.val();
}

long double& corv::Var::BasicLongDouble::val() const
{
	return v;
}
corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::val(const long double& value) const
{
	v = value;
	return (BasicLongDouble&)* this;
}
corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::val(const BasicLongDouble& value) const
{
	v = value.val();
	return (BasicLongDouble&)* this;
}

corv::Var::BasicInt corv::Var::BasicLongDouble::toCeil() const
{
	int temp = (int)v;
	if (temp < v)
		temp++;
	return temp;
}

corv::Var::BasicInt corv::Var::BasicLongDouble::toFloor() const
{
	int temp = (int)v;
	if (temp > v)
		temp++;
	return temp;
}

corv::Var::BasicInt corv::Var::BasicLongDouble::toRound() const
{
	return v < 0 ? int(v - 0.5) : int(v + 0.5);
}

corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::operator+=(const BasicLongDouble& value) const
{
	v += value.val();
	return (BasicLongDouble&)* this;
}
corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::operator-=(const BasicLongDouble& value) const
{
	v -= value.val();
	return (BasicLongDouble&)* this;
}
corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::operator*=(const BasicLongDouble& value) const
{
	v *= value.val();
	return (BasicLongDouble&)* this;
}
corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::operator/=(const BasicLongDouble& value) const
{
	v /= value.val();
	return (BasicLongDouble&)* this;
}

corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::operator=(const long double& value) const
{
	v = value;
	return (BasicLongDouble&)* this;
}
corv::Var::BasicLongDouble& corv::Var::BasicLongDouble::operator=(const BasicLongDouble& value) const
{
	v = value.val();
	return (BasicLongDouble&)* this;
}

corv::Var::BasicLongDouble::operator long double() const
{
	return v;
}

corv::Type corv::Var::BasicLongDouble::type() const
{
	return Type::LongDouble;
}

/*

********************
*Internal functions*
********************

*/

template<class I>
I corv::Var::intern_toNum(BasicString value)
{
	I n = 0;
	for (int i = 0; i < value[i] >= '0' && value[i] <= '9'; i++)
		n = 10 * n + (value[i] - '0');
	return n;
}

template<class I>
corv::Var::BasicString corv::Var::intern_base(I value, int b)
{
	int i = 0;
	bool neg = false;
	I v = value;
	BasicString s;
	if (value)
	{
		s = '0';
		return s;
	}
	if (value < 0)
	{
		neg = true;
		value = -value;
	}
	while (value != 0)
	{
		int rem = v % b;
		s += (rem > 9) ? (rem - 10) + 'a' : rem + '\0';
		value = value / b;
	}
	if (neg)
		s += '-';
	return s.rev();
}

int corv::Var::intern_val(char c)
{
	if (c >= '0' && c <= '9') {
		return (int)c - '0';
	}
	else {
		return (int)c - 'A' + 10;
	}
}

/*

*************
*Array class*
*************

*/

corv::Array::ArrayDef::ArrayTypes::ArrayTypes()
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const Int& value)
	: IntV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const Long& value)
	: LongV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const Unsigned& value)
	: UnsignedV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const UnsignedLong& value)
	: UnsignedLongV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const Short& value)
	: ShortV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const UnsignedShort& value)
	: UnsignedShortV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const Char& value)
	: CharV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const String& value)
	: StringV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const Float& value)
	: FloatV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const Double& value)
	: DoubleV(value)
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const LongDouble& value)
	: LongDoubleV(value)
{
}

corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const int& value)
	: IntV(corv::Int(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const long& value)
	: LongV(corv::Long(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const unsigned& value)
	: UnsignedV(corv::Unsigned(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const unsigned long& value)
	: UnsignedLongV(corv::UnsignedLong(value))
{
}

corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const short& value)
	: ShortV(corv::Short(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const unsigned short& value)
	: UnsignedShortV(corv::UnsignedShort(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const char& value)
	: CharV(corv::Char(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const char* value)
	: StringV(corv::String(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const float& value)
	: FloatV(corv::Float(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const double& value)
	: DoubleV(corv::Double(value))
{
}
corv::Array::ArrayDef::ArrayTypes::ArrayTypes(const long double& value)
	: LongDoubleV(corv::LongDouble(value))
{
}

bool corv::Array::ArrayDef::ArrayTypes::equalsTo(const ArrayTypes& value, const Type& t) const
{
	switch (t)
	{
	case Type::Int:
		return IntV.val() == value.IntV.val();
		break;
	case Type::Long:
		return LongV.val() == value.LongV.val();
		break;
	case Type::Unsigned:
		return UnsignedV.val() == value.UnsignedV.val();
		break;
	case Type::UnsignedLong:
		return UnsignedLongV.val() == value.UnsignedLongV.val();
		break;
	case Type::Short:
		return ShortV.val() == value.ShortV.val();
		break;
	case Type::UnsignedShort:
		return UnsignedShortV.val() == value.UnsignedShortV.val();
		break;
	case Type::Char:
		return CharV.val() == value.CharV.val();
		break;
	case Type::String:
		return StringV.val() == value.StringV.val();
		break;
	case Type::Float:
		return FloatV.val() == value.FloatV.val();
		break;
	case Type::Double:
		return DoubleV.val() == value.DoubleV.val();
		break;
	case Type::LongDouble:
		return LongDoubleV.val() == value.LongDoubleV.val();
		break;
	}
}

corv::Array::ArrayDef::ArrayTypes::~ArrayTypes()
{
}

corv::Array::ArrayDef::ArrayDef()
	: nullStatus(true)
{
}
corv::Array::ArrayDef::ArrayDef(const ArrayDef& value)
{
	nullStatus = false;
	if (value.tp == Type::Int)
		v.IntV = value.getInt();
	else if (value.tp == Type::Long)
		v.LongV = value.getLong();
	else if (value.tp == Type::Unsigned)
		v.UnsignedV = value.getUnsigned();
	else if (value.tp == Type::UnsignedLong)
		v.UnsignedLongV = value.getUnsignedLong();
	else if (value.tp == Type::Short)
		v.ShortV = value.getShort();
	else if (value.tp == Type::UnsignedShort)
		v.UnsignedShortV = value.getUnsignedShort();
	else if (value.tp == Type::Char)
		v.CharV = value.getChar();
	else if (value.tp == Type::String)
		v.StringV = value.getString();
	else if (value.tp == Type::Float)
		v.FloatV = value.getFloat();
	else if (value.tp == Type::Double)
		v.DoubleV = value.getDouble();
	else if (value.tp == Type::LongDouble)
		v.LongDoubleV = value.getLongDouble();
	tp = value.tp;
}
corv::Array::ArrayDef::ArrayDef(const Int& value)
	: v(value), tp(Type::Int), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const Long& value)
	: v(value), tp(Type::Long), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const Unsigned& value)
	: v(value), tp(Type::Unsigned), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const UnsignedLong& value)
	: v(value), tp(Type::UnsignedLong), nullStatus(false)
{
}

corv::Array::ArrayDef::ArrayDef(const Short& value)
	: v(value), tp(Type::Short), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const UnsignedShort& value)
	: v(value), tp(Type::UnsignedShort), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const Char& value)
	: v(value), tp(Type::Char), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const String& value)
	: v(value), tp(Type::String), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const Float& value)
	: v(value), tp(Type::Float), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const Double& value)
	: v(value), tp(Type::Double), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const LongDouble& value)
	: v(value), tp(Type::LongDouble), nullStatus(false)
{
}

corv::Array::ArrayDef::ArrayDef(const int& value)
	: v(corv::Int(value)), tp(Type::Int), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const long& value)
	: v(corv::Long(value)), tp(Type::Long), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const unsigned& value)
	: v(corv::Unsigned(value)), tp(Type::Unsigned), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const unsigned long& value)
	: v(corv::UnsignedLong(value)), tp(Type::UnsignedLong), nullStatus(false)
{
}

corv::Array::ArrayDef::ArrayDef(const short& value)
	: v(corv::Short(value)), tp(Type::Short), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const unsigned short& value)
	: v(corv::UnsignedShort(value)), tp(Type::UnsignedShort), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const char& value)
	: v(corv::Char(value)), tp(Type::Char), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const char* value)
	: v(corv::String(value)), tp(Type::String), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const float& value)
	: v(corv::Float(value)), tp(Type::Float), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const double& value)
	: v(corv::Double(value)), tp(Type::Double), nullStatus(false)
{
}
corv::Array::ArrayDef::ArrayDef(const long double& value)
	: v(corv::LongDouble(value)), tp(Type::LongDouble), nullStatus(false)
{
}

corv::Type corv::Array::ArrayDef::getType() const
{
	return tp;
}
corv::Var::BasicInt corv::Array::ArrayDef::getInt() const
{
	if (tp != Type::Int)
		throw(-1);
	return v.IntV;
}
corv::Var::BasicLong corv::Array::ArrayDef::getLong() const
{
	if (tp != Type::Long)
		throw(-1);
	return v.LongV;
}
corv::Var::BasicUnsigned corv::Array::ArrayDef::getUnsigned() const
{
	if (tp != Type::Unsigned)
		throw(-1);
	return v.UnsignedV;
}
corv::Var::BasicUnsignedLong corv::Array::ArrayDef::getUnsignedLong() const
{
	if (tp != Type::UnsignedLong)
		throw(-1);
	return v.UnsignedLongV;
}
corv::Var::BasicShort corv::Array::ArrayDef::getShort() const
{
	if (tp != Type::Short)
		throw(-1);
	return v.ShortV;
}
corv::Var::BasicUnsignedShort corv::Array::ArrayDef::getUnsignedShort() const
{
	if (tp != Type::UnsignedShort)
		throw(-1);
	return v.UnsignedShortV;
}
corv::Var::BasicChar corv::Array::ArrayDef::getChar() const
{
	if (tp != Type::Char)
		throw(-1);
	return v.CharV;
}
corv::Var::BasicString corv::Array::ArrayDef::getString() const
{
	if (tp != Type::String)
		throw(-1);
	return v.StringV;
}
corv::Var::BasicFloat corv::Array::ArrayDef::getFloat() const
{
	if (tp != Type::Float)
		throw(-1);
	return v.FloatV;
}
corv::Var::BasicDouble corv::Array::ArrayDef::getDouble() const
{
	if (tp != Type::Double)
		throw(-1);
	return v.DoubleV;
}
corv::Var::BasicLongDouble corv::Array::ArrayDef::getLongDouble() const
{
	if (tp != Type::LongDouble)
		throw(-1);
	return v.LongDoubleV;
}

bool corv::Array::ArrayDef::equalsTo(const ArrayDef& value) const
{
	return v.equalsTo(value.v, tp);
}

void corv::Array::ArrayDef::operator=(const ArrayDef& value) const
{
	nullStatus = false;
	if (value.tp == Type::Int)
		v.IntV = value.getInt();
	else if (value.tp == Type::Long)
		v.LongV = value.getLong();
	else if (value.tp == Type::Unsigned)
		v.UnsignedV = value.getUnsigned();
	else if (value.tp == Type::UnsignedLong)
		v.UnsignedLongV = value.getUnsignedLong();
	else if (value.tp == Type::Short)
		v.ShortV = value.getShort();
	else if (value.tp == Type::UnsignedShort)
		v.UnsignedShortV = value.getUnsignedShort();
	else if (value.tp == Type::Char)
		v.CharV = value.getChar();
	else if (value.tp == Type::String)
		v.StringV = value.getString();
	else if (value.tp == Type::Float)
		v.FloatV = value.getFloat();
	else if (value.tp == Type::Double)
		v.DoubleV = value.getDouble();
	else if (value.tp == Type::LongDouble)
		v.LongDoubleV = value.getLongDouble();
	tp = value.tp;
}

corv::Array::ArrayDef::operator corv::Var::BasicInt() const
{
	if (tp != Type::Int)
		throw(-1);
	return v.IntV;
}
corv::Array::ArrayDef::operator corv::Var::BasicLong() const
{
	if (tp != Type::Long)
		throw(-1);
	return v.LongV;
}
corv::Array::ArrayDef::operator corv::Var::BasicUnsigned() const
{
	if (tp != Type::Unsigned)
		throw(-1);
	return v.UnsignedV;
}
corv::Array::ArrayDef::operator corv::Var::BasicUnsignedLong() const
{
	if (tp != Type::UnsignedLong)
		throw(-1);
	return v.UnsignedLongV;
}
corv::Array::ArrayDef::operator corv::Var::BasicShort() const
{
	if (tp != Type::Short)
		throw(-1);
	return v.ShortV;
}
corv::Array::ArrayDef::operator corv::Var::BasicUnsignedShort() const
{
	if (tp != Type::UnsignedShort)
		throw(-1);
	return v.UnsignedShortV;
}
corv::Array::ArrayDef::operator corv::Var::BasicChar() const
{
	if (tp != Type::Char)
		throw(-1);
	return v.CharV;
}
corv::Array::ArrayDef::operator corv::Var::BasicString() const
{
	if (tp != Type::String)
		throw(-1);
	return v.StringV;
}
corv::Array::ArrayDef::operator corv::Var::BasicFloat() const
{
	if (tp != Type::Float)
		throw(-1);
	return v.FloatV;
}
corv::Array::ArrayDef::operator corv::Var::BasicDouble() const
{
	if (tp != Type::Double)
		throw(-1);
	return v.DoubleV;
}
corv::Array::ArrayDef::operator corv::Var::BasicLongDouble() const
{
	if (tp != Type::LongDouble)
		throw(-1);
	return v.LongDoubleV;
}

corv::Array::ArrayDef::operator int() const
{
	if (tp != Type::Int)
		throw(-1);
	return v.IntV.val();
}
corv::Array::ArrayDef::operator long() const
{
	if (tp != Type::Long)
		throw(-1);
	return v.LongV.val();
}
corv::Array::ArrayDef::operator unsigned() const
{
	if (tp != Type::Unsigned)
		throw(-1);
	return v.UnsignedV.val();
}
corv::Array::ArrayDef::operator unsigned long() const
{
	if (tp != Type::UnsignedLong)
		throw(-1);
	return v.UnsignedLongV.val();
}
corv::Array::ArrayDef::operator short() const
{
	if (tp != Type::Short)
		throw(-1);
	return v.ShortV.val();
}
corv::Array::ArrayDef::operator unsigned short() const
{
	if (tp != Type::UnsignedShort)
		throw(-1);
	return v.UnsignedShortV.val();
}
corv::Array::ArrayDef::operator char() const
{
	if (tp != Type::Char)
		throw(-1);
	return v.CharV.val();
}
corv::Array::ArrayDef::operator const char* () const
{
	if (tp != Type::String)
		throw(-1);
	return v.StringV.val();
}
corv::Array::ArrayDef::operator float() const
{
	if (tp != Type::Float)
		throw(-1);
	return v.FloatV.val();
}
corv::Array::ArrayDef::operator double() const
{
	if (tp != Type::Double)
		throw(-1);
	return v.DoubleV.val();
}
corv::Array::ArrayDef::operator long double() const
{
	if (tp != Type::LongDouble)
		throw(-1);
	return v.LongDoubleV.val();
}

bool corv::Array::ArrayDef::isNull() const
{
	return nullStatus;
}
corv::Array::ArrayDef& corv::Array::ArrayDef::clear() const
{
	v.nullV = true;
	nullStatus = true;
	return (ArrayDef&)* this;
}

template<class T>
corv::Array::ArrayDef::operator T() const
{
	switch (tp)
	{
	case Type::Int:
		return (T)v.IntV;
		break;
	case Type::Long:
		return (T)v.LongV;
		break;
	case Type::Unsigned:
		return (T)v.UnsignedV;
		break;
	case Type::UnsignedLong:
		return (T)v.UnsignedLongV;
		break;
	case Type::Short:
		return (T)v.ShortV;
		break;
	case Type::UnsignedShort:
		return (T)v.UnsignedShortV;
		break;
	case Type::Char:
		return (T)v.CharV;
		break;
	case Type::String:
		return (T)v.StringV;
		break;
	case Type::Float:
		return (T)v.FloatV;
		break;
	case Type::Double:
		return (T)v.DoubleV;
		break;
	case Type::LongDouble:
		return (T)v.LongDoubleV;
		break;
	}
}

corv::Array::Array()
{
}
corv::Array::Array(const ArrayDef& value)
{
	l = 1;
	v = new ArrayDef[l];
	v[0] = value;
}
corv::Array::Array(const Array& value)
{
	l = value.len();
	v = new ArrayDef[l];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
}

corv::Var::BasicInt& corv::Array::len() const
{
	return (corv::Var::BasicInt&)l;
}

corv::Array& corv::Array::append(const Array& value) const
{
	ArrayDef* temp = new ArrayDef[l + value.len()];
	for (int i = 0; i < l; i++)
		temp[i] = v[i];
	for (int i = 0; i < value.len(); i++)
		temp[i + l] = value[i];
	l += value.len();
	v = new ArrayDef[0];
	delete[] v;
	v = new ArrayDef[l];
	for (int i = 0; i < l; i++)
		v[i] = temp[i];
	return (Array&)* this;
}
corv::Array& corv::Array::append(const ArrayDef& value) const
{
	return append(Array(value));
}
template<class... T>
corv::Array& corv::Array::append(const Array& value, const T&... r) const
{
	append(value);
	return append(r...);
}
template<class... T>
corv::Array& corv::Array::append(const ArrayDef& value, const T&... r) const
{
	append(Array(value), r...);
}

corv::Array corv::Array::copyWithin(const Int& target) const
{
	return copyWithin(target, 0, l);
}

corv::Array corv::Array::copyWithin(const Int& target, const Int& f) const
{
	return copyWithin(target, f, l);
}

corv::Array corv::Array::copyWithin(const Int& target, const Int& f, const Int& t) const
{
	int value = target;
	if (value < 0)
		value = 0;
	if (value >= l)
		value = l - 1;
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= l)
		from = l - 1;
	int to = t;
	if (to < 0)
		to = 0;
	if (to > l)
		to = l;
	if (to <= from)
		return *this;
	Array res;
	int ver = 0;;
	for (int i = 0; i < value; i++)
	{
		res.append(v[i]);
		ver++;
	}
	for (int i = from; i < to; i++)
	{
		if (ver < l)
			res.append(v[i]);
		ver++;
	}
	for (int i = (to - from) + value; i < l; i++)
		res.append(v[i]);
	return res;
}

corv::Int corv::Array::indexOf(const Array& value) const
{
	int found = 0;
	for (int i = 0; i < l; i++)
		for (int ii = 0; ii < value.len(); ii++)
		{
			if (value[found].equalsTo(v[i]))
			{
				found++;
				if (found == value.len())
					return (i - value.len()) + 1;
				break;
			}
			else
				found = 0;
		}
	return -1;
}
corv::Int corv::Array::indexOf(const ArrayDef& value) const
{
	return indexOf(Array(value));
}
corv::Int corv::Array::indexOf(const Array& value, const Int& f) const
{
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= l)
		return -1;
	if (subArray(from).indexOf(value) == -1)
		return -1;
	return subArray(from).indexOf(value) + subArray(0, from).len();
}
corv::Int corv::Array::indexOf(const ArrayDef& value, const Int& f) const
{
	return indexOf(Array(value), f);
}
corv::Int corv::Array::indexOf(const Array& value, const Int& f, const Int& t) const
{
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > l)
		to = l;
	if (from >= l || from >= to || to <= 0)
		return -1;
	if (subArray(from, to).indexOf(value) == -1)
		return -1;
	return subArray(from, to).indexOf(value) + subArray(0, from).len();
}
corv::Int corv::Array::indexOf(const ArrayDef& value, const Int& f, const Int& t) const
{
	return indexOf(Array(value), f, t);
}

corv::Array corv::Array::ins(const Array& value) const
{
	Array res;
	res.append(value);
	for (int i = 0; i < l; i++)
		res.append(v[i]);
	return res;
}
corv::Array corv::Array::ins(const ArrayDef& value) const
{
	return ins(Array(value));
}
corv::Array corv::Array::ins(const Array& value, const Int& p) const
{
	int pos = p;
	if (pos < 0)
		pos += l;
	if (pos < 0)
		pos = 0;
	if (pos > l)
		pos = l;
	Array res;
	for (int i = 0; i < pos; i++)
		res.append(v[i]);
	for (int i = 0; i < value.len(); i++)
		res.append(value[i]);
	for (int i = pos; i < l; i++)
		res.append(v[i]);
	return res;
}
corv::Array corv::Array::ins(const ArrayDef& value, const Int& p) const
{
	return ins(Array(value), p);
}

corv::Int corv::Array::lastIndexOf(const Array& value) const
{
	int start = 0;
	while (true)
		if (indexOf(value, start) != -1)
			start = indexOf(value, start) + 1;
		else
			if (start)
				return -1;
			else
				return start - 1;
}
corv::Int corv::Array::lastIndexOf(const ArrayDef& value) const
{
	return lastIndexOf(Array(value));
}
corv::Int corv::Array::lastIndexOf(const Array& value, const Int& f) const
{
	return subArray(f).lastIndexOf(value);
}
corv::Int corv::Array::lastIndexOf(const ArrayDef& value, const Int& f) const
{
	return lastIndexOf(Array(value), f);
}
corv::Int corv::Array::lastIndexOf(const Array& value, const Int& f, const Int& t) const
{
	return subArray(f, t).lastIndexOf(value);
}
corv::Int corv::Array::lastIndexOf(const ArrayDef& value, const Int& f, const Int& t) const
{
	return lastIndexOf(Array(value), f, t);
}

corv::Array corv::Array::rmv(const Array& value) const
{
	return repl(value, ArrayDef());
}
corv::Array corv::Array::rmv(const ArrayDef& value) const
{
	return rmv(Array(value));
}

corv::Array corv::Array::rmvArr(const Int& f) const
{
	return subArray(0, f);
}
corv::Array corv::Array::rmvArr(const Int& f, const Int& length) const
{
	int Length = length;
	if (Length > l - f)
		Length = l - f;
	int from = f;
	if (from < 0)
		from = 0;
	int to = from + length;
	if (from >= l || from >= to || to <= 0)
		return *this;
	return subArray(0, from) + subArray(from + Length, l);
}

corv::Array corv::Array::rmvArray(const Int& f) const
{
	return subArray(0, f);
}
corv::Array corv::Array::rmvArray(const Int& f, const Int& t) const
{
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > l)
		to = l;
	if (from >= l || from >= to || to <= 0)
		return *this;
	return subArray(0, from) + subArray(to, l);
}

corv::Array corv::Array::repl(const Array& value, const Array& newValue) const
{
	int ind = 0;
	if (indexOf(value) == -1)
		return *this;
	int sl = l;
	int ol = value.len();
	int nl = newValue.len();
	bool verify = false;
	int n = 0;
	Array res;
	for (int i = 0; i < l; i++)
	{
		ArrayDef c = v[i];
		if (verify)
		{
			if (n < ol)
			{
				if (c.equalsTo(value[n]))
				{
					n++;
					if (i == sl - 1)
						res.append(newValue);
					continue;
				}
				else
				{
					for (int ii = n; ii > 0; ii--)
						res.append(v[i - ii]);
					n = 0;
					verify = false;
				}
			}
			else
			{
				res += newValue;
				n = 0;
				verify = false;
			}
		}
		if (c.equalsTo(value[0]))
		{
			if (i == sl - 1)
			{
				if (ol > 1)
					res += v[sl - 1];
				else
					res += newValue;
			}
			else
			{
				n++;
				verify = true;
				continue;
			}
		}
		else
			res += c;
		return res;
	}
}
corv::Array corv::Array::repl(const ArrayDef& value, const Array& newValue) const
{
	return repl(Array(value), newValue);
}
corv::Array corv::Array::repl(const Array& value, const ArrayDef& newValue) const
{
	return repl(value, Array(newValue));
}
corv::Array corv::Array::repl(const ArrayDef& value, const ArrayDef& newValue) const
{
	return repl(Array(value), Array(newValue));
}
corv::Array corv::Array::repl(const Array& value, const Array& newValue, const Int& f) const
{
	int from = f;
	if (from < 0)
		from = 0;
	if (from >= l)
		return *this;
	return subArray(0, f) + subArray(f).repl(value, newValue);
}
corv::Array corv::Array::repl(const ArrayDef& value, const Array& newValue, const Int& f) const
{
	return repl(Array(value), newValue, f);
}
corv::Array corv::Array::repl(const Array& value, const ArrayDef& newValue, const Int& f) const
{
	return repl(value, Array(newValue), f);
}
corv::Array corv::Array::repl(const ArrayDef& value, const ArrayDef& newValue, const Int& f) const
{
	return repl(Array(value), Array(newValue), f);
}
corv::Array corv::Array::repl(const Array& value, const Array& newValue, const Int& f, const Int& t) const
{
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > l)
		to = l;
	if (from >= l || from >= to || to <= 0)
		return *this;
	return subArray(0, f) + subArray(f, t).repl(value, newValue) + subArray(t);
}
corv::Array corv::Array::repl(const ArrayDef& value, const Array& newValue, const Int& f, const Int& t) const
{
	return repl(Array(value), newValue, f, t);
}
corv::Array corv::Array::repl(const Array& value, const ArrayDef& newValue, const Int& f, const Int& t) const
{
	return repl(value, Array(newValue), f, t);
}
corv::Array corv::Array::repl(const ArrayDef& value, const ArrayDef& newValue, const Int& f, const Int& t) const
{
	return repl(Array(value), Array(newValue), f, t);
}

corv::Array corv::Array::subArr(const Int& f) const
{
	return subArray(f);
}
corv::Array corv::Array::subArr(const Int& f, const Int& length) const
{
	int from = f;
	if (from < 0)
		from = 0;
	int Length = length;
	if (Length > l - f)
		Length = l - f;
	return subArray(from, from + Length);
}

corv::Array corv::Array::subArray(const Int& f) const
{
	int from = f;
	if (from < 0)
		from = 0;
	Array arr;
	if (from >= l)
		return arr;
	else
	{
		for (int i = from; i < l; i++)
			arr.append(v[i]);
		return arr;
	}
}
corv::Array corv::Array::subArray(const Int& f, const Int& t) const
{
	int from = f;
	if (from < 0)
		from = 0;
	int to = t;
	if (to > l)
		to = l;
	Array arr;
	if (from >= l || from >= to || to <= 0)
		return arr;
	else
	{
		for (int i = from; i < to; i++)
			arr.append(v[i]);
		return arr;
	}
}

corv::Array corv::Array::operator+(const Array& value) const
{
	Array res;
	for (int i = 0; i < l; i++)
		res.append(v[i]);
	return res.ins(value);
}
corv::Array corv::Array::operator+(const ArrayDef& value) const
{
	return operator+(Array(value));
}
corv::Array corv::Array::operator*(const Int& value) const
{
	Array res;
	for (int i = 0; i < value.val(); i++)
		for (int ii = 0; ii < l; ii++)
			res.append(v[ii]);
	return res;
}

corv::Array& corv::Array::operator+=(const Array& value) const
{
	Array res;
	for (int i = 0; i < l; i++)
		res.append(v[i]);
	return operator=(res.ins(value));
}
corv::Array& corv::Array::operator+=(const ArrayDef& value) const
{
	return operator+=(Array(value));
}
corv::Array& corv::Array::operator*=(const Int& value) const
{
	Array res;
	for (int i = 0; i < value.val(); i++)
		for (int ii = 0; ii < l; ii++)
			res.append(v[ii]);
	return operator=(res);
}
corv::Array& corv::Array::operator=(const ArrayDef& value) const
{
	return operator=(Array(value));
}

bool corv::Array::operator==(const Array& value) const
{
	if (l == value.len())
	{
		for (int i = 0; i < l; i++)
			if (!v[i].equalsTo(value[i]))
				return false;
	}
	else
		return false;
	return true;
}
bool corv::Array::operator==(const ArrayDef& value) const
{
	return operator==(Array(value));
}
bool corv::Array::operator!=(const Array& value) const
{
	return !operator==(value);
}
bool corv::Array::operator!=(const ArrayDef& value) const
{
	return !operator==(value);
}

bool corv::Array::operator>(const Array& value) const
{
	return l > value.len();
}
bool corv::Array::operator>(const ArrayDef& value) const
{
	return operator>(Array(value));
}
bool corv::Array::operator>(const Int& value) const
{
	return l > value;
}
bool corv::Array::operator<(const Array& value) const
{
	return l < value.len();
}
bool corv::Array::operator<(const ArrayDef& value) const
{
	return operator<(Array(value));
}
bool corv::Array::operator<(const Int& value) const
{
	return l < value;
}
bool corv::Array::operator>=(const Array& value) const
{
	return l >= value.len();
}
bool corv::Array::operator>=(const ArrayDef& value) const
{
	return operator>=(Array(value));
}
bool corv::Array::operator>=(const Int& value) const
{
	return l >= value;
}
bool corv::Array::operator<=(const Array& value) const
{
	return l <= value.len();
}
bool corv::Array::operator<=(const ArrayDef& value) const
{
	return operator<=(Array(value));
}
bool corv::Array::operator<=(const Int& value) const
{
	return l <= value;
}

corv::Array& corv::Array::operator=(const Array& value) const
{
	delete[] v;
	l = value.len();
	v = new ArrayDef[l];
	for (int i = 0; i < l; i++)
		v[i] = value[i];
	return (Array&)* this;
}

corv::Array::ArrayDef& corv::Array::operator[](const int& value) const
{
	int ind = value;
	if (ind < 0)
		ind += l;
	if (ind < 0)
		ind = 0;
	if (ind >= l)
		ind = l - 1;
	return (ArrayDef&)v[ind];
}

#endif // CORV_H
