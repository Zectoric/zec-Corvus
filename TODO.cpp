//   _________     _______    ________       _______
//  |___   ___|   /  ___  \   | _____ \     /  ___  \
//      | |      |  /   \  |  | |    \ \   |  /   \  |        _____    ______    ______
//      | |      | |     | |  | |     | |  | |     | |       |  ___|  | ____ |  | ____ |
//      | |      | |     | |  | |     | |  | |     | |       | |      | |  | |  | |  | |
//      | |      |  \___/  |  | |____/ /   |  \___/  |   _   | |___   | |__| |  | |__| |
//      |_|       \ _____ /   |______ /     \ _____ /   |_|  |_____|  |  ____|  |  ____|
//                                                                    | |       | |
//                                                                    |_|       |_|
//
//
// [ * ] : Done
// [ = ] : Working
// [ - ] : Pending
//
// TODO: Change style of INT and DEC classes [ * ]
// TODO: Make Array::operator[](const int&) return ANY types [ * ]
// TODO: Make functions and operators of each class that are necessary to return reference become return reference [ * ]
// TODO: Add all methods into each classes [ * ]
//     Declare private members of functions to "Var" class' below private access specifier [ * ]
//     Declare public members of functions to each class [ * ]
//     Define definitions for all public and private declared functions [ * ]
// TODO: Declare all member functions of class Array [ * ]
// TODO: Define all member functions of class Array [ = ]
// TODO: Try these methods in Demo.cpp file [ - ]
// TODO: Create Logging system by Creating the class "Log" or "BasicLog" [ - ]
// TODO: Create Error codes and exception system by Creating the class "Err" or "BasicErr" [ - ]
// TODO: Create "Obj" and "Tuple" class type [ - ]
// TODO: Git PUSH and release 1.0 [ - ]
// TODO: Due Date: Monday (2/9/2019) [ = ]
//